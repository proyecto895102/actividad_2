import { createRouter, createWebHistory } from 'vue-router'
import McQueen from '../views/McQueenView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'McQueen',
      component: () => import('@/views/McQueenView.vue')
    },
    {
      path: '/about',
      name: 'Holaa',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/',
      name: 'Boton',
      component: () => import('../views/Botonview.vue')
    },
    {
      path: '/model',
      name: 'Modelo',
      component: () => import('../views/ModelView.vue')
    },
    {
      path: '/NavbarView',
      name: 'Barra',
      component: () => import('../views/BarraView.vue')
    },
    {
      path: '/como',
      name: '56',
      component: () => import('../views/ComoView.vue')
    },
    {
      path: '/Vista2View',
      name: 'Ultimo boton',
      component: () => import('../views/Ultimobotonview.vue')
    }
  ]
})

export default router
